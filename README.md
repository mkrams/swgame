## SW Game

First, please install necessary packages, for example with npm:

`npm install`

Then, run the game with:

`npm run start`

Run tests with:

`npm run jest-test`