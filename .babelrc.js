module.exports = {
    presets: [
        ['@babel/preset-env', {targets: {node: 'current'}}],
        '@babel/preset-react'
    ],
    // presets: [
    //     ['@babel/preset-env', {targets: {node: 'current'}}],
    // ],
    env: {
        test: {
            presets: [['@babel/preset-env', {modules: 'commonjs'}]],
            "plugins": ["@babel/plugin-transform-runtime"]
        }
    },
    plugins: [
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-proposal-class-properties',
        'transform-react-remove-prop-types',
        [
            'transform-imports',
            {
                'react-router': {
                    transform: 'react-router/${member}',
                    preventFullImport: true
                }
            }
        ]
    ]
}
