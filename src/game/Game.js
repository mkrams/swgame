import React from 'react';
import {withStyles} from '@material-ui/styles';
import GameCard from '../components/GameCard';
import Button from '@material-ui/core/Button';
import {chooseTwoRandomFromResource, setWinner} from '../Utils/utilities';

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
        minWidth: '50%',
        margin: 'auto',
        boxShadow: 'none',
    },
    gameCard: {},
    chooseResource: {
        margin: '5px'
    }
};

const initialState = {
    gameOn: false,
    randomResults: [],
    resourceName: '',
    loadingResources: false,
    draw: false,
};

class Game extends React.Component {
    constructor(props) {
        super(props);

        this.state = initialState;
    }

    // Sets fetched data in the localStorage and state. If data is in localStorage, it gets it from there.
    fetchAllPagesFromSource = (resourceName) => {
        if (localStorage.getItem(`${resourceName}`)) {
            const cachedResource = JSON.parse(localStorage.getItem(`${resourceName}`));

            this.setGameCards(cachedResource, resourceName)
        } else {
            new Promise((resolve, reject) => {
                this.fetchResource(`https://swapi.co/api/${resourceName}`, [], resolve, reject);
            }).then(fetchedResource => {
                localStorage.setItem(`${resourceName}`, JSON.stringify(fetchedResource));

                this.setGameCards(fetchedResource, resourceName);
            }).catch(error =>
                console.log('Error:', error)
            )
        }
    };

    // Sets winner based on biggest value of mass/crew attribute from all the objects.
    // Winner will be written in 'winner' prop of an object.
    // Can handle arrays of objects of any length, not just two.
    // When new resource is added, there is only a need to add new case for switch statement.
    setGameCards = (gameCards, resourceName) => {
        let attributeName;
        let randomCards = chooseTwoRandomFromResource(gameCards);

        switch (resourceName) {
            case 'starships':
                attributeName = 'crew';
                break;
            case 'people':
                attributeName = 'mass';
                break;
        }

        let objectsWithSetWinner = setWinner(randomCards, attributeName);

        const occurrencesOfSameValue = randomCards.reduce((prev, curr) => prev + (curr[`${attributeName}`] > 1), 0);
        let isDraw = occurrencesOfSameValue < 1;

        this.setState({
            randomResults: objectsWithSetWinner,
            loadingResources: false,
            draw: isDraw,
        });
    };

    // Goes through all the pages of paginated data in api
    fetchResource = (url, people, resolve, reject) => {
        fetch(url)
            .then(response => response.json())
            .then(response => {
                const fetchedResource = people.concat(response.results);
                if (response.next !== null) {
                    this.fetchResource(response.next, fetchedResource, resolve, reject);
                } else {
                    resolve(fetchedResource);
                }
            })
            .catch(error => {
                console.log(`Error: ${error}`);
                reject('Something wrong. Please refresh the page and try again.')
            })
    };

    // Starts the game
    playGame = () => {
        this.setState({
            gameOn: true,
        });
    };

    // Restarts the game
    playAgain = () => {
        this.setState(initialState);
    };

    // Sets the resource and calls for data for this resource
    chooseResource = (resourceName) => {
        this.setState({
            resourceName: resourceName,
            loadingResources: true,
        });
        this.fetchAllPagesFromSource(resourceName);
    };

    render() {
        const {classes} = this.props;

        return (
            <div className={classes.container}>
                {this.state.gameOn ?
                    <div>
                        {this.state.draw && <span>DRAW!</span>}
                        {this.state.randomResults.map(resource => (
                            <GameCard key={resource.name}
                                      resourceName={this.state.resourceName}
                                      {...resource} />
                        ))}
                        <Button variant="contained"
                                onClick={() => this.playAgain()}>
                            Play again
                        </Button>
                    </div> :
                    this.state.randomResults.length ?
                        <Button variant="contained"
                                onClick={() => this.playGame()}>
                            Play the game
                        </Button> :
                        this.state.loadingResources ?
                            <span>Loading resources...</span> :
                            <>
                                <Button variant="contained"
                                        className={classes.chooseResource}
                                        onClick={() => this.chooseResource('people')}>
                                    Play with people
                                </Button>
                                <Button variant="contained"
                                        className={classes.chooseResource}
                                        onClick={() => this.chooseResource('starships')}>
                                    Play with starships
                                </Button>
                            </>
                }
            </div>
        )
    }
}

export default withStyles(styles)(Game);
