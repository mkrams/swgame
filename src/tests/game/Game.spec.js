import React from 'react';
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Game from "../../game/Game";
global.fetch = require('jest-fetch-mock');

configure({adapter: new Adapter()});

const mockBody = {
    "count": 87,
    "next": "https://swapi.co/api/people/?page=2",
    "previous": null,
    "results": [
        {
            "name": "Luke Skywalker",
            "height": "172",
            "mass": "77",
            "hair_color": "blond",
            "skin_color": "fair",
            "eye_color": "blue",
            "birth_year": "19BBY",
            "gender": "male",
        }
    ]
};

describe('Game', () => {
    fetch.mockResponseOnce(JSON.stringify(mockBody));
    const component = mount(shallow(<Game/>).get(0));

    it('chooses resource and calls for fetch', () => {
        const component = mount(shallow(<Game/>).get(0));

        component
            .find('.MuiButton-root').first()
            .simulate('click');

        let state = component.state();
        expect(state.loadingResources).toBeTruthy();
        expect(state.resourceName).toMatch('people' || 'starships');
        expect(global.fetch).toHaveBeenCalled();
    });
});
