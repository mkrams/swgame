import {chooseTwoRandomFromResource, setWinner, checkDraw} from '../../Utils/utilities';
import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

describe('Utilitles', () => {
    it('Sets winner', () => {
        const mock = [
            {
                "name": "Luke Skywalker",
                "height": "172",
                "mass": "77",
                "hair_color": "blond",
                "skin_color": "fair",
                "eye_color": "blue",
                "birth_year": "19BBY",
                "gender": "male",
            },
            {
                "name": "C-3PO",
                "height": "167",
                "mass": "75",
                "hair_color": "n/a",
                "skin_color": "gold",
                "eye_color": "yellow",
                "birth_year": "112BBY",
                "gender": "n/a",
            },
        ];

        const bodyWithWinnerSet = setWinner(mock);
        expect(bodyWithWinnerSet[0]).toHaveProperty('winner');
    });
    it('Chooses two randomly', () => {
        const mock = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];

        let firstRandoms = chooseTwoRandomFromResource(mock);
        let secondRandoms = chooseTwoRandomFromResource(mock);

        expect(firstRandoms).not.toEqual(secondRandoms);
    });
    it('Checks draw', () => {
        const mock = [
            {a: 1},
            {a: 1}
        ];

        let isDraw = checkDraw(mock);

        expect(isDraw).toBe(true);
    });
});