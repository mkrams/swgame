import React from 'react';
import {withStyles} from "@material-ui/core";
import Card from '@material-ui/core/Card';
import {Starship} from './Starship';
import {Person} from './Person';

const styles = {
    ordinary: {
        minWidth: '45%',
        margin: '5px',
        backgroundColor: '#ff8227',
    },
    winner: {
        minWidth: '45%',
        margin: '5px',
        animation: '$winnerAnimation 1s infinite'
    },
    '@keyframes winnerAnimation': {
        '0%': {
            backgroundColor: '#ff8227',
        },
        '50%': {
            backgroundColor: 'green',
        },
        '100%': {
            backgroundColor: '#ff8227',
        },
    }
};

class GameCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            winner: props.winner,
        }
    }

    render() {
        const {classes} = this.props;

        return (
            <>
                <Card className={this.props.winner ? classes.winner : classes.ordinary}>
                    {this.props.resourceName === 'starships' && <Starship {...this.props} />}
                    {this.props.resourceName === 'people' && <Person {...this.props} />}
                </Card>
                {this.props.draw && <span>DRAW!</span>}
            </>
        );
    }
}

export default withStyles(styles)(GameCard);
