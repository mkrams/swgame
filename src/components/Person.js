import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

export const Person = (props) => {
    return (
        <CardContent>
            <Typography variant="h6">
                {props.name}
            </Typography>
            <Typography variant="body1">
                Height: {props.height}
            </Typography>
            <Typography variant="body1">
                Mass: {props.mass}
            </Typography>
            <Typography variant="body1">
                Gender: {props.gender}
            </Typography>
            <Typography variant="body1">
                Birth date: {props.birth_year}
            </Typography>
        </CardContent>
    )
};