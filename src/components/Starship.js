import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

export const Starship = (props) => {
    return (
            <CardContent>
                <Typography variant="h6">
                    {props.name}
                </Typography>
                <Typography variant="body1">
                    Model: {props.model}
                </Typography>
                <Typography variant="body1">
                    Class: {props.starship_class}
                </Typography>
                <Typography variant="body1">
                    Crew: {props.crew}
                </Typography>
                <Typography variant="body1">
                    Cost in credits: {props.cost_in_credits}
                </Typography>
            </CardContent>
    )
};