const chooseTwoRandomFromResource = (resource) => {
    let randomFromResource = [];

    while (randomFromResource.length < 2) {
        let randomResource = resource[Math.floor(Math.random() * resource.length)];

        if (!randomFromResource.includes(randomResource)) {
            randomFromResource.push(randomResource);
        }
    }
    return randomFromResource;
};

const setWinner = (randomObjects, attributeName) => {
    const maxValue = randomObjects.reduce((prev, curr) => {
        return prev[`${attributeName}`] > curr[`${attributeName}`] ? prev[`${attributeName}`] : curr[`${attributeName}`]
    });

    for (const object of randomObjects) {
        object['winner'] = object[`${attributeName}`] === maxValue;
    }

    return randomObjects;
};

const checkDraw = (objects, attributeName) => {
    const occurrencesOfSameValue = objects.reduce((prev, curr) => prev + (curr[`${attributeName}`] > 1), 0);
    return occurrencesOfSameValue < 1;
};

module.exports = {
    chooseTwoRandomFromResource,
    setWinner,
    checkDraw
};